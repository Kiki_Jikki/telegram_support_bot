#!/usr/bin/env python
# -*- coding: utf-8 -*-

import telebot 
from telebot import types
from config import BOT_TOKEN,bxtoken
bot = telebot.TeleBot(BOT_TOKEN)
import requests
from bitrix24 import *
import datetime
from pymongo import MongoClient

now = datetime.datetime.now()
bx24 = Bitrix24(bxtoken)
client = MongoClient('localhost', 27017)
db = client.opros
collsipid = db.sip_id

@bot.message_handler(content_types=["text"])

def start(message):
    if  message.text.lower() == "привет":
        msg = bot.send_message(message.chat.id, "Я искусственный интеллект Вася. Скажи пароль?")  
        bot.register_next_step_handler(msg, f1)
        
def f1(message):
    if message.text.lower() == "кактус":
       msg = bot.send_message(message.chat.id, "Откуда ты? напиши база или магазин?")
       bot.register_next_step_handler(msg, f2)

def f2(message):
    global local,project
    local = message.text.lower()

#--- по деволту проект в битриксе ТП магазинов - общая группа
    project = "41"
    if local == "база":
        project = "42"
    msg = bot.send_message(message.chat.id, "Напиши название города или отдела:")
    bot.register_next_step_handler(msg, f4)

def f4(message):
    global mag
    
    mag = message.text.lower()
    query = { "name": mag }
    for value in collsipid.find(query):
        if value:
            if 'project' in value:
                project = str(value['project'])

#---- добавляем кнопки выбора
    markup = types.ReplyKeyboardMarkup(row_width=1)
    itembtn1 = types.KeyboardButton('Проблемы пк не связанные с 1с')
    itembtn2 = types.KeyboardButton('Телефония')
    itembtn3 = types.KeyboardButton('Видеонаблюдение')
    itembtn4 = types.KeyboardButton('не работает функция в 1с')
    itembtn5 = types.KeyboardButton('Тормозит 1с')
    itembtn6 = types.KeyboardButton('Не получается разобраться в 1с')
    itembtn7 = types.KeyboardButton('Принтер или картридж')
    itembtn8 = types.KeyboardButton('Почта')
    itembtn9 = types.KeyboardButton('ЭДО или Битрикс')
    itembtn10 = types.KeyboardButton('Терминалы сбора данных')
    markup.add(itembtn1, itembtn2, itembtn3, itembtn4, itembtn5, itembtn6, itembtn7, itembtn8, itembtn9, itembtn10)
    msg = bot.send_message(message.chat.id, "Прокрути вниз и выбери тему заявки:", reply_markup=markup)
    bot.register_next_step_handler(msg, f5)

def f5(message):
    global header
    header = str(message.text.lower())

    msg = bot.send_message(message.chat.id, "Имя твоего компьютера?")
    bot.register_next_step_handler(msg, f6)


def f6(message):
    global pc
    pc = message.text.lower()
    msg = bot.send_message(message.chat.id, "Контактный номер телефона?")
    bot.register_next_step_handler(msg, f7)

def f7(message):

    global tel
    tel = message.text.lower()

    msg = bot.send_message(message.chat.id, "Приложите фотографию проблемы или напишите слово да или нет")
    bot.register_next_step_handler(msg, f77)

def f77(message):
    global photos
    if message.text.lower() == "нет":
       photos = 0
       msg = bot.send_message(message.chat.id, f"Нужно описание проблемы")
       bot.register_next_step_handler(msg, f8)
    else:
       photos = 1
       msg = bot.send_message(message.chat.id, f"Жду фотографию. Если будет с компа, поставьте галочку - сжать фото")
       bot.register_next_step_handler(msg, f777)

def f777(message):
    global raw,downloaded_file,path,path1
    raw = message.photo[2].file_id
    directory = "/var/www/telegram/"
    path = directory + raw + ".jpg"
    path1 = raw + ".jpg"
    file_info = bot.get_file(raw)
    downloaded_file = bot.download_file(file_info.file_path)
    filed = str(downloaded_file)
    with open (path, 'wb') as new_file:
         new_file.write(downloaded_file)
         bot.send_message(message.chat.id, f"Нужно описание проблемы.")
         bot.register_next_step_handler(message, f8)

def f8(message):
    global description, quest,logstr
    
    description = message.text.lower()
    if photos == 0:
        quest = description + " \n отдел:  " + mag + " \n телефон: "+ tel + " \n имя компьютера: " + pc 
    else:
        quest = description + " \n отдел:  " + mag + " \n телефон: "+ tel + " \n имя компьютера: " + pc + " \n Ссылка на фотографию: \n " + "http://172.16.0.22:5000/" + path1
    
    
    bot.send_message(message.chat.id, f"Заявка создана. Мы искренне надеемся, что все решится само, но все равно в ближайщее время мы перезвоним")
    date_time = str(now.strftime("%d-%m-%y %H:%M"))
    date_time2 = now.strftime("%y-%m-%d")
    date_time2 = str(date_time2 + " 20:00")
    telega_id = str(message.chat.id)
    logstr = date_time + " - telegram_id: " + telega_id  + quest
    supportchat = "-684309685"

    bot.send_message(supportchat, logstr)
    f = open('/var/log/telegrambot/support_bot.log', 'a')

    f.write(logstr)
    f.close


#---- подключаемся к битриксу и создаем заявку
    try:
        bx24.callMethod('tasks.task.add', fields={
            'TITLE': header, 
            'DESCRIPTION': quest, 
            'CREATED_BY': 732, 
            'RESPONSIBLE_ID': 732,
            'AUDITORS': [732],
            'GROUP_ID': project, 
            'DEADLINE': date_time2 })
    except BitrixError as error1:
        print(message)


bot.polling(none_stop=True)

